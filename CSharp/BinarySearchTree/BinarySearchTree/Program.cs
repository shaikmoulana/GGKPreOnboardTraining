﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaySearchTree
{
    class Node
    {
        public int data;
        public Node right;
        public Node left;
    }
    class Tree
    {
        public Node InsertValue(Node root, int number)
        {
            if (root == null)
            {
                root = new Node
                {
                    data = number
                };
            }
            if (number < root.data)
            {
                root.left = InsertValue(root.left, number);
            }
            if (number > root.data)
            {
                root.right = InsertValue(root.right, number);
            }
            return root;
        }
        //Ascending Order
        public void AscendingOrder(Node root)
        {
            if (root != null)
            {
                AscendingOrder(root.left);
                Console.Write(root.data + "  ");
                AscendingOrder(root.right);
            }
        }
        //Descending Order
        public void DescendingOrder(Node root)
        {
            if (root != null)
            {
                DescendingOrder(root.right);
                Console.Write(root.data + "  ");
                DescendingOrder(root.left);
            }
        }
        //Search
        public bool SearchValue(Node root, int Value)
        {
            if (root == null)
            {
                return false;
            }
            else
            {
                if (root.data == Value)
                {
                    return true;
                }
                else if (Value < root.data)
                {
                    return SearchValue(root.left, Value);
                }
                else
                {
                    return SearchValue(root.right, Value);
                }
            }

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Tree Bst = new Tree();
            Node root = null;

            /*root = Bst.InsertValue(root, 7);
            root = Bst.InsertValue(root, 3);
            root = Bst.InsertValue(root, 11);
            root = Bst.InsertValue(root, 1);
            root = Bst.InsertValue(root, 5);
            root = Bst.InsertValue(root, 9);
            root = Bst.InsertValue(root, 13);
            root = Bst.InsertValue(root, 4);
            root = Bst.InsertValue(root, 6);
            root = Bst.InsertValue(root, 8);
            root = Bst.InsertValue(root, 12);
            root = Bst.InsertValue(root, 14);
            bool k = Bst.SearchValue(root, 90);
            if(k)
            {
                Console.WriteLine("Number 12 Found.");
            }
            else
            {
                Console.WriteLine("Number 12 Not found.");
            }
            Bst.AscendingOrder(root);
            Console.WriteLine();
            Bst.DescendingOrder(root);*/
            Console.WriteLine("Enter Number of Nodes");
            int NodeNum = int.Parse(Console.ReadLine());
            for (int i = 0; i < NodeNum; i++)
            {
                root = Bst.InsertValue(root, int.Parse(Console.ReadLine()));
            }
            Console.WriteLine("1. Search\n 2. Ascending Order\n 3. Descending Order\n");
            int option = int.Parse(Console.ReadLine());
            switch (option)
            {
                case 1:
                    int number = int.Parse(Console.ReadLine());
                    if (Bst.SearchValue(root, number))
                    {
                        Console.WriteLine("Value Found");
                    }
                    else
                    {
                        Console.WriteLine("Value Not Found");
                    }
                    break;
                case 2:
                    Bst.AscendingOrder(root);
                    break;
                case 3:
                    Bst.DescendingOrder(root);
                    break;
                default:
                    Console.WriteLine("Invalid Input");
                    break;
            }
            Console.ReadKey();
        }
    }
}
