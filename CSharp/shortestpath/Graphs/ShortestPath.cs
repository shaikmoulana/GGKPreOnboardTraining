﻿/* https://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/ */
using Graphs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    public class ShortestPath
    {
        public int MAX = 9999;
        public int[] Distance;
        
        public StringBuilder FinalPath = new StringBuilder();
        public void printSolution(int[] Parent, int destination)
        {
            if (Parent[destination] == -1)
            {
                return;
            }
            printSolution(Parent, Parent[destination]);
            FinalPath.Append(destination);
            
        }
        public int minDistance(int[] Distance, bool[] pickIndex)
        {
            int min = MAX;
            int minIndex=0;
            int v = Distance.Length;
            for(int i = 0; i < v; i++)
            {
                if(pickIndex[i]==false && (Distance[i] <= min))
                {
                    min = Distance[i];
                    minIndex = i;
                }
            }
            return minIndex;
        }
        public void Dijkstra(int[,] Weight, int source, int destination)
        {
            int vertices = Weight.GetLength(0);
            Distance=new int[vertices];//the shortest distance from source to some Value;
            bool[] pickIndex = new bool[vertices];//pickIndex[i] will true if vertex i is included in path;
            int[] Parent = new int[vertices];
            Parent[source] = -1;
            for (int i = 0; i < vertices; i++)
            {
                Distance[i] = MAX;
                pickIndex[i] = false;
            }
            Distance[source] = 0;
            
            for(int i = 0; i < vertices-1; i++)
            {
                int minD = minDistance(Distance, pickIndex);
                pickIndex[minD] = true;
                for(int j = 0; j < vertices; j++)
                {
                    if(!(pickIndex[j]) && (Weight[minD, j]!=-1) && (Distance[minD]+Weight[minD, j] < Distance[j]))
                    {
                        Parent[j]= minD;
                        Distance[j] = Distance[minD] + Weight[minD, j];
                    }
                }
            }
            FinalPath.Append(source);
            printSolution(Parent, destination);
        }        

    }
}
