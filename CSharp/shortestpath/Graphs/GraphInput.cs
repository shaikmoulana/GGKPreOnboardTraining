﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    
    class GraphInput
    {
        struct Vertex
        {
            public string name;
        }
        public int vertices;
        public int[,] Weights;
        public int[] visited;
        Vertex[] VList;
        
        public GraphInput(int v)
        {
            vertices = v;
            Weights = new int[v, v];
            visited = new int[v];
            VList = new Vertex[v];
            for (int i = 0; i < v; i++)
            {
                for (int j = 0; j < v; j++)
                {
                    Weights[i, j] = -1;
                }
            }

        }
        public void LabelVertex(int index, string label)
        {
            VList[index] = new Vertex();
            VList[index].name = label;
        }
        public int getLabel(string label)
        {
            int index = -1;
            for(int i = 0; i < vertices; i++)
            {
                if (VList[i].name == label)
                {
                    index = i;

                }

            }
            return index;
        }
        public string getIndex(int label)
        {
            return VList[label].name;
        }
        public void addEdge(string v, string w, int weight)
        {
            int v1 = getLabel(v);
            int w1 = getLabel(w);
            Weights[v1, w1] = weight;
            Weights[w1, v1] = weight;
        }
        //Prints DFS Traversal;
        public void DFS(int source)
        {
            Console.Write(getIndex(source) + "->");
            visited[source] = 1;
            for (int i = 0; i < vertices; i++)
            {
                if (visited[i] != 1 && Weights[i, source] != -1)
                {
                    DFS(i);
                }
            }
        }
    }
}
