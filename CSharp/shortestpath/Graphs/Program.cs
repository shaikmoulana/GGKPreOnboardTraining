﻿using Graphs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    class Graphs
    {
        static void Main(string[] args)
        {
            GraphInput graph = new GraphInput(10);
            graph.LabelVertex(0, "1");
            graph.LabelVertex(1, "2");
            graph.LabelVertex(2, "3");
            graph.LabelVertex(3, "4");
            graph.LabelVertex(4, "5");
            graph.LabelVertex(5, "6");
            graph.LabelVertex(6, "9");
            graph.LabelVertex(7, "12");
            graph.LabelVertex(8, "10");
            graph.LabelVertex(9, "13");

            graph.addEdge("1", "2", 1);
            graph.addEdge("2", "5", 3);
            graph.addEdge("5", "9", 4);
            graph.addEdge("9", "10", 1);
            graph.addEdge("10", "13", 3);
            graph.addEdge("13", "12", 2);
            graph.addEdge("12", "4", 8);
            graph.addEdge("4", "3", 1);
            graph.addEdge("3", "1", 2);
            graph.addEdge("4", "6", 2);
            graph.addEdge("5", "6", 1);
            graph.addEdge("12", "9", 3);
            
            ShortestPath S = new ShortestPath();
            Console.WriteLine("Enter the Source Node");
            string source1 = Console.ReadLine();
            int Source = graph.getLabel(source1);
            
            Console.WriteLine("Enter the Destination Node");
            string destination1 = Console.ReadLine();
            int Destination = graph.getLabel(destination1);
            
            
            Console.Write("The Shortest Path is ");
            S.Dijkstra(graph.Weights, Source, Destination);
            
            int L = S.FinalPath.Length;
            
            for(int i = 0; i < L; i++)
            {
                int P=int.Parse((S.FinalPath[i].ToString()));
                if (i <= L - 2)
                {
                    Console.Write(graph.getIndex(P) + "->");
                }
                else
                {
                    Console.Write(graph.getIndex(P));
                }
            }
            
            Console.WriteLine(" with total Distance "+S.Distance[Destination]);
            Console.ReadKey();
        }
    }
}
